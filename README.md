# leapfi


   ![](https://github.com/nu11secur1ty/leapfi/blob/master/Leap-green-254x300.png) 
                  - Wifi attack software for Open SUSE Leap15
-------------------------------------------------------------------------------------------------------------------



# INSTALLATION: 
***WARNING: YOU MUST INSTALL THIS SOFTWARE STEP BY STEP!!!***

- Login as root

```bash 
cd /opt/
git clone https://github.com/nu11secur1ty/leapfi.git
cp leapfi/packages/packman.inode.at-suse_1.repo /etc/zypp/repos.d/
zypper -n up
zypper -n dup
***manual installing all packages***
bash leapfi/packages/pack.sh
bash leapfi/install/install.sh
```

# Test

```bash
:~ # leapfi
```
- Output:
```bash
                        @
                       
                 @   @  @  @ @ @  @ @ @
                 @ @ @  @    @    @@ @ 
                 @   @  @    @    @
         W I F I @   @  @    @    @ @ @
 code_name_leapfi_(#f0r OpenSUSE Leap 15 VERSION= 2018.01)

             copyright ® nu11secur1ty 


-------------------Main Menu---------------------
Choice wlan interface.
!Please check all wifi devices before you start!
[11]  wlan0
[01]  Wlan1 Devices-RECOMMENDED!
[02]  Wlan2 Devices-RECOMMENDED!
[03]  Wlan3 Devices-RECOMMENDED!
[04]  Check for connected victims
[05]  Check for update
[!] Press enter to close if you don't want to use this program.
------------------------------------------------------------
            WARNING! If you do not use wlan0 press Ctrl+C.     
           Then connect your device, and run program again!  
          --------------------------------------------------

         After use this program, you have to reboot the system, this is RECOMMENDED!  
          ---------------------------------------------------

         Using WIFI Interfaces.
eth0      no wireless extensions.

wlan0     IEEE 802.11  ESSID:"nu11secur1ty"  
          Mode:Managed  Frequency:2.432 GHz  Access Point: 00:00:00:00:00:00   
          Bit Rate=121.5 Mb/s   Tx-Power=15 dBm   
          Retry short limit:7   RTS thr:off   Fragment thr:off
          Encryption key:off
          Power Management:off
          Link Quality=70/70  Signal level=-22 dBm  
          Rx invalid nwid:0  Rx invalid crypt:0  Rx invalid frag:0
          Tx excessive retries:0  Invalid misc:108   Missed beacon:0

lo        no wireless extensions.


         Please make your choice...

```

# Happy hunting with nu11secur1ty ;)



                                             
